import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import NavBar from "./pages/NavBar";
import Footer from './pages/Footer';
import { useState } from 'react';
import { Client as Styletron } from 'styletron-engine-atomic';
import { Provider as StyletronProvider } from 'styletron-react';
import { LightTheme, BaseProvider } from 'baseui';
import CaeDemandes from './application/cae/CaeDemandes';
import Contact from './pages/Contact';
import ListeDemandes from './application/cae/listDemandes';
import ListePrincipale from './application/cae/ListePrincipale';
import ListeAttente from './application/cae/ListeAttente';
import ChangeRoom from './application/aa/ChangeRoom';
import ValiderLog from './application/aa/ValiderLog';
import AaAccueil from './application/aa/AaAccueil';


/*baseui css style generator */
const engine = new Styletron();


function App() {
  return (

    <div className="App">

      <StyletronProvider value={engine}>
        <BaseProvider theme={LightTheme}>
          <BrowserRouter>
            <NavBar />
            <Routes>
              <Route exact path='/' element={<Home  />} />
              <Route exact path='/contact' element={<Contact  />} />
              <Route path='/admin/cae/accueil' element={<CaeDemandes  />} />
              <Route path='/admin/cae/demandes' element={<ListeDemandes  />} />
              <Route path='/admin/cae/listP' element={<ListePrincipale  />} />
              <Route path='/admin/cae/listA' element={<ListeAttente  />} />

              <Route path='/admin/aa/accueil' element={<AaAccueil  />} />
              <Route path='/admin/aa/valider' element={<ValiderLog  />} />
              <Route path='/admin/aa/chambres' element={<ChangeRoom  />} />
            </Routes>
            <Footer />

          </BrowserRouter>
        </BaseProvider>
      </StyletronProvider>
    </div>
  );
}
export default App;