import '../css/navbar.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { brands, solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import logo1 from './onouscLogo.png'
import logo2 from './eduSupGov.png'
import Login from './Login';

function NavBar() {
  return (
    <div>
      <header className='headerNav container-fluid' >
        <div className="row justify-content-around">
          <div className="logo1 col-md-4 mt-2 ">
            <img src={logo1} alt='logo onousc' />
          </div>
          <div className="logo2 col-md-3 mt-2">
            <img src={logo2} alt='logo onousc' />
          </div>
        </div>

      </header>

      <nav class="navbar navbar-expand-lg navbar-light sticky-top px-4 px-lg-5" style={{ background: 'transparent' }}>
        <a href="index.html" class="navbar-brand d-flex align-items-center">
          <h1 class="m-0"><img class="img-fluid me-3" src="img/icon/icon-02-primary.png" alt="" /></h1>
        </a>
        <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse" style={{ marginLeft: "5em" }}>
          <div class="navbar-nav mx-auto bg-light pe-4 py-3 py-lg-0">
            <Link to="/" class="nav-item nav-link active">Accueil</Link>
            <Link to="/" class="nav-item nav-link">Historique</Link>
            <Link to="/" class="nav-item nav-link">Services</Link>
            <Link to="/" class="nav-item nav-link">publication</Link>
            <div class="nav-item dropdown">
              <Link to="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Espace Administratif</Link>
              <div class="dropdown-menu bg-light border-0 m-0 p-2">
                <Login />
              </div>
            </div>
            <Link to="/contact" class="nav-item nav-link">Contact</Link>
          </div>
          <div class="h-100 d-lg-inline-flex align-items-center d-none">
            <Link class="btn btn-square rounded-circle bg-light text-primary me-2" to="/"><i><FontAwesomeIcon icon={brands('facebook')} /></i></Link>
            <Link class="btn btn-square rounded-circle bg-light text-primary me-2" to="/"><i><FontAwesomeIcon icon={solid('phone')} /></i></Link>
            <Link class="btn btn-square rounded-circle bg-light text-primary me-2" to="/"><i><FontAwesomeIcon icon={solid('message')} /></i></Link>
            <Link class="btn btn-square rounded-circle bg-light text-primary me-0" to="/"><i><FontAwesomeIcon icon={solid('location-arrow')} /></i></Link>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default NavBar