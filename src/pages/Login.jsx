import React, { useState } from 'react'
import "../css/login.css";
import { Link, useNavigate  } from 'react-router-dom';
import axios from 'axios';

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate ();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post("http://localhost:8000/api/login", {
        email: email,
        password: password,
      });
  
      if (response.data.success) {
        localStorage.setItem("access_token", "your_access_token_here");
        navigate("/admin/cae/accueil");
      } else {
        setError("Invalid email or password");
      }
    } catch (error) {
      setError("An error occurred during login");
    }
  };

  axios.interceptors.request.use(
    (config) => {
      const accessToken = localStorage.getItem("access_token");
  
      if (accessToken) {
        config.headers["Authorization"] = `Bearer ${accessToken}`;
      }
  
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  return (
    <section className='loginSection' >

      <div class="form-box">
        <div class="form-value">
          <form onSubmit={handleSubmit}>
            <h2>Connexion</h2>
            <div class="inputbox">
              <input type="email" required  value={email} onChange={handleEmailChange} />
              <label for="">Email</label>
            </div>
            <div class="inputbox">
              <input type="password" required  value={password} onChange={handlePasswordChange} />
              <label for="">Mot de passe</label>
            </div>
            <div class="forget">
              <label class="loginOptions">
                <div className='mr-1'><input type="checkbox" />Resté connecter</div>
                <Link to="/PasswordRecovery">Mot de passe oublié</Link>
              </label>
            </div>
              <button type="submit">Se Connecter</button>
            {error && <p className='alert alert-danger text-dark mt-3'>{error}</p>}

            <div class="register">
            </div>
          </form>
        </div>
      </div>
    </section>
  )
}

export default Login