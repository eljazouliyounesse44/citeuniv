import React from 'react'
import "../css/home.css";
import ServicesSummary from './HomePage/ServicesSummary';
import News from './HomePage/News';
import Location from './HomePage/Location';
import Facts from './HomePage/Facts';
import Login from './Login';
const Home = () => {
  return (
    <div className='homeBody'>
      <div>
        <ServicesSummary />
        <Facts />
        <News />
        <Location />
      </div>

    </div>
  )
}

export default Home;