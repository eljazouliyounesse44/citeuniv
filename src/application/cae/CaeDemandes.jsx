import "../../css/dashboard.css"
import React, { useEffect, useState } from 'react'
import { styled } from 'baseui';
import Sidebar from './SidebarC';
import { Data } from "./Data";
import * as XLSX from 'xlsx'
import axios from 'axios';
import Swal from 'sweetalert2';

const CaeDemandes = () => {
    const [open, setOpen] = React.useState(false);
    const [excelFile, setExcelFile] = useState(null);
    const [excelFileError, setExcelFileError] = useState(null);
    const [validationError, setValidationError] = useState({})
    const [testData, setTestData] = useState(null)
    // submit
    const [excelData, setExcelData] = useState(null);
    // it will contain array of objects

    // handle File
    const fileType = ['application/vnd.ms-excel'];
    const handleFile = (e) => {
        let selectedFile = e.target.files[0];
        if (selectedFile) {
            // console.log(selectedFile.type);
            if (selectedFile && fileType.includes(selectedFile.type)) {
                let reader = new FileReader();
                reader.readAsArrayBuffer(selectedFile);
                reader.onload = (e) => {
                    setExcelFileError(null);
                    setExcelFile(e.target.result);
                }
            }
            else {
                setExcelFileError('Please select only excel file types');
                setExcelFile(null);
            }
        }
        else {
            console.log('plz select your file');
        }
    }

    // submit function
    const handleSubmit =  (e) => {
        e.preventDefault();

        if (excelFile !== null) {
            const workbook = XLSX.read(excelFile, { type: 'buffer' });
            const worksheetName = workbook.SheetNames[0];
            const worksheet = workbook.Sheets[worksheetName];
            const data = XLSX.utils.sheet_to_json(worksheet);
            setExcelData(data);
            console.log(excelData);

        }
        else {
            setExcelData(null);
        }
    }

    const saveItems = async () => {
        // const newArray = excelData.map(({ Nom, Prenom, cnie, "code massar": codeMassar1, dateNaissance, gendre, nationalité, "Province des parents": provinceParents1, etablissment, diplomePrepare, cycleEtude, niveauEtude, 'nouvelResident(oui/non)': nouvelResident1, "nombreeAnnée de résidence": nombreAnnee1, "revenue Anuelle des parents": revenueAnuelle1, handicapé }) =>
        // ({
        //     Nom: Nom, Prenom: Prenom, cin: cnie, codeMassar: codeMassar1, dateNaissance: dateNaissance, gender: gendre, nationalite: nationalité, provinceParents: provinceParents1,
        //     etablissement: etablissment, diplomePrepare: diplomePrepare, cycleEtudes: cycleEtude, niveauEtudes: niveauEtude, nouvelResident: nouvelResident1, nombreAnnee: nombreAnnee1,
        //     revenueAnuelle: revenueAnuelle1, handicapé: handicapé
        // }));
        await axios.post(`http://localhost:8000/api/data`, { data: excelData })
            .then(({ data }) => {
                Swal.fire({
                    icon: "success",
                    text: data.message
                })
            }).catch(({ response }) => {
                if (response.status === 422) {
                    setValidationError(response.data.errors)
                } else {
                    Swal.fire({
                        text: response.data.message,
                        icon: "error"
                    })
                }
            })
    }

    return (
        <DashboardWrapper>
            <Sidebar open={open} setOpen={setOpen} />
            <div className="container-fluid">
                {/* upload file section */}
                <div className='form'>
                    <form className='form-group' autoComplete="off"
                        onSubmit={handleSubmit}>
                        <label><h5>Importer votre fichier</h5></label>
                        <br></br>
                        <input type='file' className='form-control'
                            onChange={handleFile} required></input>
                        {excelFileError && <div className='text-danger'
                            style={{ marginTop: 5 + 'px' }}>{excelFileError}</div>}
                        <button type='submit'  className="btn btn-outline-dark mx-5"
                            style={{ marginTop: 15 + 'px',backgroundColor:"#fa9715" }}>Voir les données</button>
                    </form>
                </div>

                <hr></hr>

                {/* view file section */}
                <h5>View Excel file </h5>
                <div className='viewer'>
                    {excelData === null && <>No file selected</>}
                    {excelData !== null && (
                        <div className='table-responsive'>
                            <button onClick={saveItems} style={{"backgroundColor":"#fa9715"}} className="btn btn-outline-dark mx-5">Enregistrer les demandes exporter à la base de données</button>
                            <table className='table table-light table-hover'>
                                <thead>
                                    <tr>
                                        <th scope='col'>Nom</th>
                                        <th scope='col'>Prenom</th>
                                        <th scope='col'>CIN</th>
                                        <th scope='col'>Revenue Anuelle</th>
                                        <th scope='col'>Nouvelle Résident</th>
                                        <th scope='col'>Handicapé</th>
                                        <th scope='col'>Nombre Année</th>
                                        <th scope='col'>Province des Parents</th>
                                        <th scope='col'>Date Naissance</th>
                                        <th scope='col'>Gendre</th>
                                        <th scope='col'>Nationalité</th>
                                        <th scope='col'>Etablissment Fréquenté</th>
                                        <th scope='col'>Diplome préparé</th>
                                        <th scope='col'>Cycle d'études</th>
                                        <th scope='col'>Niveau d'études</th>
                                        <th scope='col'>code Massar</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <Data excelData={excelData} />
                                </tbody>
                            </table>
                        </div>
                    )}
                </div>

            </div>
        </DashboardWrapper>
    )
}

export default CaeDemandes

const DashboardWrapper = styled('section', {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    background: '#ffff',
    position: 'relative',
    paddingLeft: '285px',
    paddingRight: '2rem',
    width: '100%',
    minHeight: '100vh',
    maxWidth: '100vw',
    boxSizing: 'border-box',

    '@media (max-width: 768px)': {
        paddingLeft: '0',
    }
});