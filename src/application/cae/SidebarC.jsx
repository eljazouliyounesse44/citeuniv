import React from 'react'

import { styled, useStyletron } from 'baseui';

import SideNavListItem from './SideNavListItems';



const Sidebar = ({ open, setOpen }) => {

    const [css] = useStyletron();

    return (
        <SidebarWrapper className={css({
            backgroundColor: '#fa9715',
            '@media (max-width: 768px)': {
                display: open ? 'block' : 'none',
            }
        })}>
            <div className={css({
                position: 'fixed',
                top: '0',
                left: '0',
                width: '100vw',
                background: 'rgba(0, 0, 0, 0.5)',
                height: '100vh',
                zIndex: '-1',
                display: 'none',

                '@media (max-width: 768px)': {
                    display: open ? 'block' : 'none',
                }
            })}
                onClick={() => setOpen(false)}
            />
            <Logo>
                <img className={css({
                    width: '2rem',
                    height: '2rem',
                    marginRight: '0.5rem',
                })} src={"images/3.jpg"} alt="logo" />
                CURA TABLEAU DE BBORD
            </Logo>


            <SideNavListItem />

        </SidebarWrapper>
    )
}

export default Sidebar

const SidebarWrapper = styled('section', {
    position: 'fixed',
    top: '0',
    left: '0',
    width: '100%',
    maxWidth: '255px',
    height: '100vh',
    background: 'repeating-radial-gradient(circle at center center, transparent 0px, transparent 11px,rgba(255,255,255,0.04) 11px, rgba(255,255,255,0.04) 19px,transparent 19px, transparent 29px,rgba(255,255,255,0.04) 29px, rgba(255,255,255,0.04) 33px),repeating-radial-gradient(circle at center center, rgb(250,151,21) 0px, rgb(250,151,21) 5px,rgb(250,151,21) 5px, rgb(250,151,21) 17px,rgb(250,151,21) 17px, rgb(250,151,21) 30px,rgb(250,151,21) 30px, rgb(250,151,21) 43px,rgb(250,151,21) 43px, rgb(250,151,21) 45px,rgb(250,151,21) 45px, rgb(250,151,21) 47px); background-size: 53px 53px',
    zIndex: '1',
    overflowX: 'hidden',
});

const Logo = styled('div', {
    padding: '2.5rem 2rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '1.25rem',
    color: '#f2f2f2',
    fontWeight: 'bold',
    boxSizing: 'border-box',
    background: 'none',
})