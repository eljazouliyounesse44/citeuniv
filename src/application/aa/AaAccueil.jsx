import "../../css/dashboard.css"
import React, { useEffect, useState } from 'react'
import { styled } from 'baseui';
import Sidebar from './SidebarC';
import axios from 'axios';
import Swal from 'sweetalert2';

const AaAccueil = () => {
    const [open, setOpen] = React.useState(false);


    return (
        <DashboardWrapper>
            <Sidebar open={open} setOpen={setOpen} />
            <div className="container-fluid">
                    <h1>This is just test</h1>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nisi laboriosam laborum in tempora dicta tempore praesentium deleniti cum perspiciatis impedit ipsum laudantium eum magnam, atque aperiam expedita voluptates voluptatem architecto.</p>
            </div>
        </DashboardWrapper>
    )
}

export default AaAccueil

const DashboardWrapper = styled('section', {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    background: '#ffff',
    position: 'relative',
    paddingLeft: '325px',
    paddingRight: '2rem',
    width: '100%',
    minHeight: '100vh',
    maxWidth: '100vw',
    boxSizing: 'border-box',

    '@media (max-width: 768px)': {
        paddingLeft: '0',
    }
});