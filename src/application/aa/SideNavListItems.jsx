import React, { useState } from 'react'
import { FaArrowCircleLeft, FaExclamationTriangle, FaListAlt, FaStopCircle, FaDatabase ,FaFileExcel } from 'react-icons/fa';
import { styled } from 'baseui';
import { Link } from 'react-router-dom';

const SideNavListItem = () => {
    const [activeMenuItem, setActiveMenuItem] = useState("exporterDemandes");

    const handleMenuItemClick = (menuItem) => {
      setActiveMenuItem(menuItem);
    };
    return (

        <div className='sideBarLinks'>
            <StyledMenuItem $active={activeMenuItem === 'exporterDemandes'}>
                <Link onClick={() => handleMenuItemClick('exporterDemandes')} to="/admin/aa/accueil"> 
                    <FaFileExcel style={{ marginRight: '0.5rem' }} />Accueil
                </Link>
            </StyledMenuItem>
            <StyledMenuItem $active={activeMenuItem === 'listeDemandes'}>
                <Link onClick={() => handleMenuItemClick('listeDemandes')} to="/admin/aa/valider"> 
                    <FaFileExcel style={{ marginRight: '0.5rem' }} />Validation de logement
                </Link>
            </StyledMenuItem>
            <StyledMenuItem $active={activeMenuItem === 'listePrincipale'}>
                <Link onClick={() => handleMenuItemClick('listePrincipale')} to="/admin/aa/chambres">
                    <FaListAlt style={{ marginRight: '0.5rem' }} />Changement de Chambre
                </Link>
            </StyledMenuItem>
            <StyledMenuItem $active={activeMenuItem === 'accueil'}>
                <Link onClick={() => handleMenuItemClick('accueil')} to="/">
                    <FaArrowCircleLeft style={{ marginRight: '0.5rem' }} /> Déconnexion
                </Link>
            </StyledMenuItem>
        </div>

            
    )
}

export default SideNavListItem

const StyledMenuItem = styled('div', props => ({
    padding: '1.25rem 1.5rem',
    background: props.$active ? '#fff' : '15d8fa',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    fontSize: '1rem',
    fontWeight: '800',
    color: props.$active ? '#fa9715' : '#FFF',
    cursor: 'pointer',
    width: '100%',
    borderLeft: props.$active ? '4px solid #DDE2FF' : 'none',

    ':hover': {
        background: '#FFF',
        color: '#fa9715',
        borderLeft: '4px solid #DDE2FF',
    }
}))