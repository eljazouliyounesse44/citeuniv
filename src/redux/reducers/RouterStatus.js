const initState = {
    views: true,
}


export const RouterStatus = (state = initState,{type , payload}) => {
    switch(type){
        case  "ADMIN_VIEWS":
            return state.views = false

        case "PUBLIC_VIEWS" :
            return state.views = true

        default : 
            return state;
    }
}
