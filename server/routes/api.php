<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListDemandesController;
use App\Http\Controllers\ListPrincipaleController;
use App\Http\Controllers\ListAttenteController;
use App\Http\Controllers\AuthController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::middleware('auth:api')->group(function () {
//     //  routes that require authentification : 
    
// });
Route::post('/login', [AuthController::class, 'login']);
Route::get('/addUser', [AuthController::class, 'addUser']);


Route::resource('listeDemandes',ListDemandesController::class);
Route::resource('listP',ListPrincipaleController::class);
Route::resource('listA',ListAttenteController::class);

Route::post('/data',[ListDemandesController::class,'storeAllData']);

Route::get('/addToListP/{id}',[ListDemandesController::class,'addToListeP']);
Route::get('/addToListA/{id}',[ListDemandesController::class,'addToListeA']);
Route::get('/listeDemandes/deleteFD/{a1}',[ListDemandesController::class,'deleteFalseDemandes']);
Route::get('/listeDemandes/gestionAuto/{a1}',[ListDemandesController::class,'gestionAuto']);

Route::get('/returnTodemandes/{id}',[ListPrincipaleController::class,'returnTodemandes']);
Route::get('/transferAuListeAttente/{id}',[ListPrincipaleController::class,'transferAuListeAttente']);

Route::get('/returnTodemandes1/{id}',[ListAttenteController::class,'returnTodemandes1']);
Route::get('/transferAuListePrincipale/{id}',[ListAttenteController::class,'transferAuListePrincipale']);
// Route::post('/login',function(Request $request) {
//     return dd($request->all());
// });



// Route::middleware('auth:api')->group(function () {
//     // Protected routes go here
//     Route::resource('listeDemandes',ListDemandesController::class);
//     Route::post('/data',[ListDemandesController::class,'storeAllData']);
// });