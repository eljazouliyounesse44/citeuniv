<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function addUser(){
        $user = new User;
        $user->name = 'Admin';
        $user->email = 'admin.cae@um5s.ma';
        $user->typeUser = 'cae';
        $user->email_verified_at = now();
        $user->password = Hash::make('admin');
        $user->remember_token = Str::random(10);
        $user->save();
    }
}
